/**

 * Title:        trap_webber.script
 * Description:  webber

 */

//------------------------------------------------
// Includes
//------------------------------------------------

include ai.ai_combat;
include library.combat;
include library.prose;
include library.ai_lib;
include library.utils;

//------------------------------------------------
// Inherits
//------------------------------------------------

inherits item.trap.trap_base;

//------------------------------------------------
// Constants
//------------------------------------------------

const int 			TRAP_DIFF 						= 15;

const string_id		SID_SYS_EFFECT					= new string_id( "trap/trap", "trap_state_def_1_effect" );
const string_id		SID_NO_EFFECT					= new string_id( "trap/trap", "trap_effect_no" );

//------------------------------------------------
// Methods
//------------------------------------------------

//------------------------------------------------
// OnAttach
//------------------------------------------------

trigger OnAttach()
{
	if (!hasObjVar(self, "droid_trap"))
	{
		setObjVar( self, "trapDiff", TRAP_DIFF );
		setObjVar( self, "trapName", "state_def_1" );
		setCount( self, 5 );
	}
	return SCRIPT_CONTINUE;
}

//------------------------------------------------
// trapHit
//------------------------------------------------

messageHandler trapHit()
{
	if ( params == null )
		return super.trapHit(self, params);

	obj_id target = params.getObjId( "target" );
	if ( (target == null) || (target == obj_id.NULL_ID) )
		return super.trapHit(self, params);
	if ( !ai_lib.isMonster( target ) || isIncapacitated( target ) || isDead( target ) )
		return super.trapHit(self, params);

	// Get player.
	obj_id player = params.getObjId( "player" );

	if ( utils.hasScriptVar( target, "trapmod.stun_defense" ) || utils.hasScriptVar( target, "trapmod.intimidate_defense" ) || utils.hasScriptVar( target, "trapmod.dizzy_defense" ) )
	{
		prose_package pp = prose.getPackage( SID_NO_EFFECT, target );
		sendSystemMessageProse( player, pp );
		return super.trapHit(self, params);		
	}

	// Affect the target.
	dictionary outparams = new dictionary();
	outparams.put( "intimidate_defense", 35 );
	outparams.put( "stun_defense", 25 );
	outparams.put( "dizzy_defense", 20 );
	outparams.put( "prefix", "state_def_1" );
	messageTo( target, "applyModTrap", outparams, 0.f, false );
	prose_package pp = prose.getPackage( SID_SYS_EFFECT, target );
	sendSystemMessageProse( player, pp );

	// If they don't have an enemy, make them attack us.
	if (!hasObjVar(self, "droid_trap"))
	{
		if ( !ai_lib.isInCombat( target ) )
			startCombat( target, player );
	}
	else
	{
		if ( !ai_lib.isInCombat( target ) )
			startCombat( target, self );
	}

	// Grant XP.
	float chance = params.getFloat( "chance" );
	grantTrapXP( player, target, 1.2f );

	return super.trapHit(self, params);
}
