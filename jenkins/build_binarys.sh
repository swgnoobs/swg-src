#!/bin/bash

basedir=$PWD
PATH=$PATH:$basedir/build/bin

DBSERVICE=
DBUSERNAME=
DBPASSWORD=
HOSTIP=
CLUSTERNAME=
NODEID=
DSRC_DIR=
DATA_DIR=

rm -rf build

if [ ! -d $basedir/build ]
then
        mkdir $basedir/build
fi

cd $basedir/build
cmake $basedir/src -DCMAKE_BUILD_TYPE=Release
make -j$(nproc)
cd $basedir
